# Welcome
To run the project please follow the below steps

 1.  run  `composer install`  to generate depedencies in vendor folder
2.  change  `.env.example`  to  `.env`
3.  run  `php artisan key:generate`
4.  configure  `.env`
5. run `php artisan migrate`
6. run `php artisan serve`
 
