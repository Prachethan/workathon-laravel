<?php

use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    if (session('token')) return redirect(route('web.products.index'));
    return view('welcome');
})->name('/');


Route::get('/login', function () {
    if (session('token')) return redirect(route('web.products.index'));
    return view('login');
});

Route::post('register', [AuthController::class, 'register'])->name('register');
Route::post('login', [AuthController::class, 'login'])->name('login');

Route::group(['middleware' => 'check_session'], function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');


    Route::resource('products', ProductController::class, ['as' => 'web']);
});
