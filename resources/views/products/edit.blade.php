@extends('layout.app')
@section('content')

@can('update-products')
<section class="container">

    <div class="row">
        <h2 class="justify-content-center">Edit Product</h2>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <form role="form" action="{{route('web.products.update',$product['id'])}}" method="post">
        @csrf
        @method('PUT')

        <div class="mb-3">
            <label for="name" class="form-label">Product Name</label>
            <input type="text" name="name" value="{{$product['name']}}" class="form-control" id="name" minlength="3" maxlength="100" required>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" name="description" id="description" required minlength="5" maxlength="250">{{$product['description']}}</textarea>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="number" name="price" value="{{$product['price']}}" class="form-control" id="price" min="0" required>
        </div>

        <div class="mb-3">
            <a href="{{route('web.products.index')}}" class="btn btn-info btn-block float-right">Back</a>
            <button type="submit" class="btn btn-primary btn-block float-right" id="submit">Update Product</button>
        </div>
    </form>
</section>
@else
<div class="alert alert-danger">
    <p>This action is unauthorized</p>
</div>
@endcan
@endsection