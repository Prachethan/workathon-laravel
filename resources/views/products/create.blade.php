@extends('layout.app')
@section('content')

@can('add-products')
<section class="container">

    <div class="row">
        <h2 class="justify-content-center">Add Product</h2>
    </div>
    @include('layout.message')
    <form role="form" action="{{route('web.products.store')}}" method="post">
        @csrf

        <div class="mb-3">
            <label for="name" class="form-label">Product Name</label>
            <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" minlength="3" maxlength="100" required>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" id="description" name="description" required minlength="5" maxlength="250">{{old('description')}}</textarea>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="number" value="{{old('price')}}" name="price" class="form-control" id="price" min="0" required>
        </div>

        <div class="mb-3">
            <a href="{{route('web.products.index')}}" class="btn btn-info btn-block float-right">Back</a>
            <button type="submit" class="btn btn-primary btn-block float-right" id="submit">Add Product</button>
        </div>
    </form>
</section>
@else
<div class="alert alert-danger">
    <p>This action is unauthorized</p>
</div>
@endcan
@endsection