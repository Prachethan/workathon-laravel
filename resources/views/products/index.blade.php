@extends('layout.app')

@section('content')
@can('add-products')
<section class="container">
    <div class="row pull-left">
        <h3>Products</h3>
    </div>

    <div class="pull-right">
        <a class="btn btn-primary" href="{{route('web.products.create')}}"> Create New Product</a>
    </div>




    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    @if( isset($products) && count($products))
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Product</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$product['name']}}</td>
                <td>{{$product['description']}}</td>
                <td>{{$product['price']}}</td>
                <td>
                    <a href="{{route('web.products.edit',$product['id'])}}">Edit</a>&nbsp;&nbsp;
                    <a href="{{route('web.products.destroy',$product['id'])}}">Delete</a>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p class="justify-content-center">No Products Added!</p>
    @endif
</section>
@else
<div class="alert alert-danger">
    <p>This action is unauthorized</p>
</div>
@endcan

@endsection