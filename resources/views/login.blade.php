@extends('layout.app')
@section('content')
<section class="container">

    <div class="row">
        <h2 class="justify-content-center">Login</h2>
    </div>
    @include('layout.message')

    <form role="form" action="{{route('login')}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" name="email" class="form-control" id="email" required>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" name="password" class="form-control" id="password" required>
        </div>

        <div class="mb-3">
            <button type="submit" class="btn btn-primary btn-block float-right" id="submit">Login</button>
        </div>
        <a href="{{route('/')}}">Create an account</a>
    </form>
</section>
@endsection