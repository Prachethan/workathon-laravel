<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username" => "required|min:3|max:100",
            "phone" => "required|min:3|max:100",
            "email" => "required|min:3|max:100|email|unique:App\Models\User,email",
            "password" => "required|min:8|max:100",
            "confirm_password" => "required|min:8|max:100|same:password",
            "role" => [
                "required",
                Rule::in(['admin', 'manager', 'staff'])
            ],

        ];
    }
}
