<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\Web\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)
            ->first();
        if (is_null($user) || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'message' => 'Invalid Credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $user->token = $user->createToken('Admin')->plainTextToken;

        return response()->json([
            "message" => "Logged in successfull!",
            "data" => $user
        ], Response::HTTP_OK);
    }
    public function register(RegisterRequest $request)
    {

        $user = User::create([
            "username" => $request->username,
            "phone" => $request->phone,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "role" => $request->role
        ]);

        Auth::login($user);
        $user->token   = $user->createToken('_token')->plainTextToken;

        return response()->json([
            'message' => 'User Created',
            'data' => $user,

        ], Response::HTTP_CREATED);
    }
}
