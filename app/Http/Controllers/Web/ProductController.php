<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->module_view_path = 'products.';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        if (Gate::allows('view-products')) {
            $req = Request::create('/api/products', 'GET');
            $req->headers->set('Accept', 'application/json');
            $res = app()->handle($req);

            if ($res->getStatusCode() === HttpFoundationResponse::HTTP_UNAUTHORIZED) {
                return redirect(route('login'))->with('danger', 'Please Login');
            }

            $data['products'] = $res->getStatusCode() === HttpFoundationResponse::HTTP_OK
                ? json_decode($res->getContent(), TRUE)['data']
                : [];
        }
        return view($this->module_view_path . 'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module_view_path . 'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        if (!Gate::allows('add-products'))
            abort(HttpFoundationResponse::HTTP_UNAUTHORIZED);

        $req = Request::create('/api/products', 'POST', $request->validated());
        $req->headers->set('Accept', 'application/json');
        app()->handle($req);
        return redirect(route('web.products.index'))
            ->with('success', 'Product Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        abort_if(!$product, 'Product doesn\'t exsists!');
        return view($this->module_view_path . 'edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        if (!Gate::allows('view-products'))
            abort(HttpFoundationResponse::HTTP_UNAUTHORIZED);


        $req = Request::create('/api/products/' . $product['id'], 'POST', $request->validated());
        $req->headers->set('Accept', 'application/json');

        $res = app()->handle($req);

        dd($res);

        return redirect(route('web.products.index'))
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
