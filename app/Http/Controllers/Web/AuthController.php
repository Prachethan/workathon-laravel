<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\Web\RegisterRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {

        $req = Request::create('/api/register', 'POST', $request->validated());
        $req->headers->set('Accept', 'application/json');

        $res = app()->handle($req);
        if ($res->getStatusCode() === Response::HTTP_CREATED) {
            $res = json_decode($res->getContent(), TRUE);
            Session::put($res['data']);
            return redirect(route('web.products.index'));
        } else {
            return redirect(route('/'));
        }
    }
    public function login(LoginRequest $request)
    {

        $req = Request::create('/api/login', 'POST', $request->validated());
        $req->headers->set('Accept', 'application/json');

        $res = app()->handle($req);
        if ($res->getStatusCode() === Response::HTTP_OK) {
            $res = json_decode($res->getContent(), TRUE);
            Session::put($res['data']);

            return redirect(route('web.products.index'));
        } else {
            return redirect(route('/'));
        }
    }
    public function logout()
    {
        Session::flush();
        return redirect(route('login'));
    }
}
