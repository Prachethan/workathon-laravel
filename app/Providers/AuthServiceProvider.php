<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-products', function ($user = null) {
            return in_array(session('role'), [User::ROLE_MANAGER, User::ROLE_ADMIN]);
        });
        Gate::define('add-products', function ($user = null) {
            return session('role') === User::ROLE_ADMIN;
        });
        Gate::define('update-products', function ($user = null) {
            return in_array(session('role'), [User::ROLE_MANAGER, User::ROLE_ADMIN]);
        });

        Gate::define('delete-products', function ($user = null) {
            return session('role') === User::ROLE_ADMIN;
        });
    }
}
